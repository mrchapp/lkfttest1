#!/bin/bash

set -eu

DIR="$(readlink -e "$(dirname "$0")")"

for stage in \
  01-kernel.sh \
  02-build.sh \
  03-artifacts.sh \
  04-publish.sh \
  05-gentests.sh \
  06-sendtests.sh \
; do
  echo
  echo "========================================"
  echo "Running stage ${stage}..."
  "${DIR}/${stage}"
done
