#!/bin/bash

source "$(readlink -e "$(dirname "$0")")/lib-lkft.sh"
lkft_init

PUB_PROTOCOL=$(echo "${PUB_DEST}" | grep -o '.*://' | sed -e 's#://##')

case "${PUB_PROTOCOL}" in
  ssh)
    ssh_noprot="$(echo "${PUB_DEST}" | sed -e 's#ssh://##')"
    SSH_SERVER="$(echo "${ssh_noprot}" | cut -d/ -f1)"
    # Slash prefixed:
    ssh_unresolved_path="/$(echo "${ssh_noprot}" | cut -d/ -f2- | sed -e 's#{#${#g')"
    SSH_PATH="$(eval echo "${ssh_unresolved_path}")"

    if [ -v SSH_PRIVATE_KEY ]; then
      old_options="$-"; set +x
      eval $(ssh-agent -s)
      ssh-add <(echo "${SSH_PRIVATE_KEY}" | tr ':' '\n')
      mkdir -p "${HOME}/.ssh"
      chmod 700 "${HOME}/.ssh"
      echo "${SSH_HOST_KEY_LINE}" > "${HOME}/.ssh/known_hosts"
      set -"${old_options}"
    fi

    ssh "${SSH_SERVER}" mkdir -p "${SSH_PATH}"
    rsync -axv "${BUILD_IMAGES}/" "${SSH_SERVER}:${SSH_PATH}/"
    ;;
esac
