# STAGES
1) Prepare kernel
2) Build LKFT/OE (kernel, kselftests, ltp, rootfs, etc.)
3) Prepare binaries (for flashing, for publishing)
4) Publish binaries (on snapshots.l.o, on SSH server)
5) Generate tests
6) Submit tests


## PREPARE KERNEL
### Needs
* env:KERNEL_REPO
* env:KERNEL_REVISION
* env:KERNEL_CONFIG (optional)
* env:KERNEL_FRAGMENTS (optionals)

### Provides
* Kernel source directory
* env:KERNEL_DESCRIBE
* env:KERNEL_MAKEVERSION
* env:KERNEL_VERSION (major.minor)

## Build LKFT/OE
### Needs
* env:MACHINE
* env:SRCREV_kernel (same as KERNEL_COMMIT)
* env:KERNEL_RECIPE
* env:KERNEL_VERSION
* sstate-cache
* downloads

### Provides
* Build images for kernel, bootloader, root file system, DTB's, etc.

## Prepare binaries
### Needs
* Build images for kernel, bootloader, root file system, DTB's, etc.
* env:MACHINE

## Publish binaries
### Needs
* env:PUB_DEST
* env:PUBLISH_SERVER
* env:LLP_GROUP (optional)

## Generate tests
### Needs
* env:MACHINE
* env:TEST_SUITES

### Provides
* YAML test jobs

## Submit tests
### Needs
* YAML test jobs

### Provides
* Test job ID's
