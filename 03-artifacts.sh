#!/bin/bash

# 03-artifacts.sh
#
# This will handle, convert and accomodate artifacts so that they are
# useful for the different systems that require them.
#
#
# Input:
#   MACHINE (environment variable)
#   Build images
#
# Output:
#   Build images (corrected)

source "$(readlink -e "$(dirname "$0")")/lib-lkft.sh"
lkft_init

# Prepare files to publish
rm -f "${BUILD_IMAGES}"/*.txt
find "${BUILD_IMAGES}" -type l -delete

# FIXME: IMAGE_FSTYPES_remove doesn't work
rm -f "${BUILD_IMAGES}"/*.rootfs.ext4 \
      "${BUILD_IMAGES}"/*.rootfs.iso \
      "${BUILD_IMAGES}"/*.rootfs.wic* \
      "${BUILD_IMAGES}"/*.iso \
      "${BUILD_IMAGES}"/*.stimg

# FIXME: Sparse and converted images here, until it gets done by OE
case "${MACHINE}" in
  juno)
    ;;
  intel-core2-32|intel-corei7-64)
    for rootfs in "${BUILD_IMAGES}"/*.hddimg; do
      if [ -e "${rootfs}" ]; then
        xz -T0 ${rootfs}
      fi
    done
    ;;
  *)
    for rootfs in "${BUILD_IMAGES}"/*.rootfs.ext4.gz; do
      if [ -e "${rootfs}" ]; then
        pigz -d -k ${rootfs}
        ext2simg -v ${rootfs%.gz} ${rootfs%.ext4.gz}.img
        rm -f ${rootfs%.gz}
        pigz -9 ${rootfs%.ext4.gz}.img
      fi
    done
    ;;
esac

# Create MD5SUMS file
find "${BUILD_IMAGES}" -type f | xargs md5sum > MD5SUMS.txt
sed -i "s|${BUILD_IMAGES}/||" MD5SUMS.txt
mv MD5SUMS.txt "${BUILD_IMAGES}"
