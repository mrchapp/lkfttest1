#!/bin/bash

# $1: Variable to check for existence
# $2: Whether to return with error code (optional);
#     must be set to "error" if so wished.
# If no arguments are provided, return with error
function check_var
{
  [ $# -lt 1 ] && return 1

  param="$1"
  reterr=""
  [ $# -ge 2 ] && reterr="$2"

  if [ ! -v "${param}" ]; then
    echo "WARNING: Variable $1 is undefined."
    # Return code 1 if set to error out
    [ "${reterr}" = "error" ] && return 1
  else
    echo "${param}: ${!param}"
  fi

  return 0
}

function lkft_init()
{
  set -eu

  [ -f "$(pwd)/build.conf" ] && CONF="$(pwd)/build.conf"
  if [ $# -gt 1 ] && [ -f "$1" ]; then CONF="$1"; fi
  if [ -v CONF ]; then
    set -a
    source "${CONF}"
    set +a
  fi

  [ -v WORKSPACE ] || WORKSPACE="/oe"
  [ -v PERMSTOR ] ||  PERMSTOR="/data"
  [ -v CI_PROJECT_DIR ] && WORKSPACE="${CI_PROJECT_DIR}"
  [ -v LKFT_CONF ] || LKFT_CONF="${WORKSPACE}/out/lkft.conf"

  export WORKSPACE PERMSTOR LKFT_CONF

  [ ! -d "${WORKSPACE}/out" ] && mkdir -p "${WORKSPACE}/out"

  # Interface with user
  if [ -e "${LKFT_CONF}" ]; then
    set -a
    echo "Contents of lkft.conf:"
    cat "${LKFT_CONF}"
    source "${LKFT_CONF}"
    set +a
  fi
}

# Use timestamps (with set -x) to better understand how the
# system behaves.
export PS4='[\t] '
