#!/bin/bash

source "$(readlink -e "$(dirname "$0")")/lib-lkft.sh"
lkft_init

# $1: file to locate
# $2: build_images directory
# $3: web base url pattern
function image_to_web()
{
  file="$(find "$2/" -name "$1" | head -n1)"
  relative_file="$(echo "${file}" | sed -e "s#$2/##")"
  web_dest=$(echo "$3" | sed -e 's#{#${#g')
  echo "$(eval echo "${web_dest}")/${relative_file}"
}

# $1: variable to define
# $2: value of that variable
function lkft_config_write()
{
  lkft_config="$(readlink -e "${LKFT_CONF}")"

  if grep -q "^$1=" "${lkft_config}"; then
    # Replace value
    sed -i -e "s#^$1=.*#$1=$2#" "${lkft_config}"
  else
    # Add value
    echo "$1=$2" >> "${lkft_config}"
  fi
}

check_var MACHINE error

pub_dest=$(echo "${PUB_DEST}" | sed -e 's#{#${#g')
web_dest=$(echo "${WEB_DEST}" | sed -e 's#{#${#g')
lkft_config_write KERNEL_CONFIG_URL "$(image_to_web config "${BUILD_IMAGES}" "${WEB_DEST}")"
lkft_config_write KERNEL_DEFCONFIG_URL "$(image_to_web defconfig "${BUILD_IMAGES}" "${WEB_DEST}")"
lkft_config_write BUILD_URL "local"
lkft_config_write BASE_URL "$(eval echo "${web_dest}")"
lkft_config_write PUB_DEST "$(eval echo "${pub_dest}")"
lkft_config_write KERNEL_URL "$(image_to_web *Image*.bin "${BUILD_IMAGES}" "${WEB_DEST}")"
lkft_config_write DTB_URL "$(image_to_web *.dtb "${BUILD_IMAGES}" "${WEB_DEST}")"
lkft_config_write NFSROOTFS_URL "$(image_to_web rpb-console-image-lkft-${MACHINE}-*.rootfs.tar.xz "${BUILD_IMAGES}" "${WEB_DEST}")"
lkft_config_write EXT4_IMAGE_URL "$(image_to_web rpb-console-image-lkft-${MACHINE}-*.rootfs.ext4 "${BUILD_IMAGES}" "${WEB_DEST}")"

case "${MACHINE}" in
  am57xx-evm)
    lkft_config_write SYSTEM_URL "$(image_to_web rpb-console-image-lkft-${MACHINE}-*.img.gz "${BUILD_IMAGES}" "${WEB_DEST}")"
    ;;
  intel-corei7-64)
    lkft_config_write HDD_URL "$(image_to_web rpb-console-image-lkft-${MACHINE}-*.hddimg.xz "${BUILD_IMAGES}" "${WEB_DEST}")"
    ;;
  juno)
    lkft_config_write RECOVERY_IMAGE_URL "$(image_to_web juno-oe-uboot.zip "${BUILD_IMAGES}" "${WEB_DEST}")"
    lkft_config_write DTB_URL "$(image_to_web *Image*-r2*.dtb "${BUILD_IMAGES}" "${WEB_DEST}")"
    ;;
esac

if [ ! -d configs ]; then
  git clone https://git.linaro.org/ci/job/configs.git
else
  cd configs
  git pull --rebase
  cd -
fi

# Mapping for MACHINE -> DEVICE_TYPE
case "${MACHINE}" in
  intel-core2-32)
    DEVICE_TYPE=i386
    ;;
  intel-corei7-64)
    DEVICE_TYPE=x86
    ;;
  juno)
    DEVICE_TYPE=juno-r2
    ;;
  am57xx-evm)
    DEVICE_TYPE=x15
    ;;
  dragonboard-410c)
    DEVICE_TYPE=dragonboard-410c
    ;;
esac
export DEVICE_TYPE
export DRY_RUN=true
# FIXME: These are a tad random:
export BUILD_NUMBER=1
export LAVA_SERVER=https://lkft.validation.linaro.org/RPC2/
export QA_SERVER=https://staging-qa-reports.linaro.org
export QA_SERVER_PROJECT=linux-mainline-oe
# By default, generate/run no tests at all
[ ! -v TEST_SUITES ] && TEST_SUITES="none"

if [ "${TEST_SUITES}" = "none" ]; then
  echo "No TEST_SUITES. Skipping generation of tests."
  exit
fi

set -a
source "${LKFT_CONF}"
set +a
cd configs/openembedded-lkft/
bash submit_for_testing.sh

# submit_for_testing.sh with DRY_RUN=true creates tmp/
if [ -d tmp ]; then
  rm -rf "${WORKSPACE}/out/lava-jobs"
  mv tmp/ "${WORKSPACE}/out/lava-jobs"
fi
