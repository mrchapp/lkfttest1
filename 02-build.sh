#!/bin/bash

# 02-build.sh
#
# This script will build LKFT/OE
#
#
# Input:
#   MACHINE (environment variable)
#   KERNEL_COMMIT (environment variable)
##   KERNEL_VERSION (environment variable)
# or:
#   MACHINE (environment variable)
#   lkft.conf (file)

source "$(readlink -e "$(dirname "$0")")/lib-lkft.sh"
lkft_init

[ -v OE_CACHE_DIR ] || OE_CACHE_DIR="${PERMSTOR}/oe"
DEFAULT_MANIFEST_URL="https://github.com/96boards/oe-rpb-manifest"
DEFAULT_MANIFEST_BRANCH="lkft/sumo"
FORCE_RINIT=""
DISTRO="lkft"

#check_var KERNEL_VERSION
check_var KERNEL_COMMIT
check_var KERNEL_REPO
check_var MACHINE error
check_var LKFT_IMAGES


# Add MACHINE to lkft.conf
grep -q ^"MACHINE=" "${LKFT_CONF}" || echo "MACHINE=${MACHINE}" >> "${LKFT_CONF}"

[ ! -v LKFT_IMAGES ] && export LKFT_IMAGES="rpb-console-image-lkft"
# Add LKFT_IMAGES to lkft.conf
grep -q ^"LKFT_IMAGES=" "${LKFT_CONF}" || echo "LKFT_IMAGES=${LKFT_IMAGES}" >> "${LKFT_CONF}"

function bitbaker()
{
  if [ -v BB_SAVELOG ] && [ "${BB_SAVELOG}" = "1" ]; then
    bitbake "$@" | tee "${WORKSPACE}/out/build-$(date +%Y%m%d-%H%M%S)"
  else
    bitbake "$@"
  fi
}

# For Bitbake
kernel_repo=$(echo "${KERNEL_REPO}" | sed -e 's#\(ssh://\|https\?://\)#git://#')
kernel_protocol=$(echo "${KERNEL_REPO}" | grep -o '.*://' | sed -e 's#://##')

DEFAULT_KERNEL_CONFIG="defconfig"
case "${MACHINE}" in
  am57xx-evm)
    DEFAULT_KERNEL_CONFIG="multi_v7_defconfig"
    ;;
  intel-core2-32)
    DEFAULT_KERNEL_CONFIG="i386_defconfig"
    ;;
  intel-corei7-64)
    DEFAULT_KERNEL_CONFIG="x86_64_defconfig"
    ;;
esac
[ -v KERNEL_CONFIG ] || KERNEL_CONFIG=${DEFAULT_KERNEL_CONFIG}

#export SRCREV_kernel="${KERNEL_COMMIT}"
export KERNEL_COMMIT
export MACHINE
export KERNEL_CONFIG
export KERNEL_RECIPE="linux-generic"
export KERNEL_VERSION="git"

# This is how the linux-generic_git.bb recipe receives the kernel
# parameters.
cat << EOF > "${WORKSPACE}/custom-kernel-info.inc.tmp"
KERNEL_COMMIT = "${KERNEL_COMMIT}"
KERNEL_REPO = "${kernel_repo}"
KERNEL_PROTOCOL = "${kernel_protocol}"
KERNEL_CONFIG_aarch64 = "${KERNEL_CONFIG}"
KERNEL_CONFIG_arm = "${KERNEL_CONFIG}"
KERNEL_CONFIG_x86-64 = "${KERNEL_CONFIG}"
KERNEL_CONFIG_x86 = "${KERNEL_CONFIG}"
EOF

# ----
REPO="$(which repo ||:)"
if [ -z "${REPO}" ]; then
  REPO="${HOME}/bin/repo"
  mkdir -p "$(dirname ${REPO})"
  [ -f "${REPO}" ] || wget https://storage.googleapis.com/git-repo-downloads/repo -O "${REPO}"
  chmod +x "${REPO}"
fi

[ -v MANIFEST_URL ] || MANIFEST_URL="${DEFAULT_MANIFEST_URL}"
[ -v MANIFEST_BRANCH ] || MANIFEST_BRANCH="${DEFAULT_MANIFEST_BRANCH}"

[ ! "${MANIFEST_URL}" = "${DEFAULT_MANIFEST_URL}" ] && FORCE_RINIT=1
[ ! "${MANIFEST_BRANCH}" = "${DEFAULT_MANIFEST_BRANCH}" ] && FORCE_RINIT=1
[ ! -d "${WORKSPACE}/.repo" ] && FORCE_RINIT=1

cd "${WORKSPACE}"
if [ -n "${FORCE_RINIT}" ]; then
  "${REPO}" init -b "${MANIFEST_BRANCH}" -u "${MANIFEST_URL}"
fi
"${REPO}" sync --force-sync

mkdir -p "${OE_CACHE_DIR}/downloads" "${OE_CACHE_DIR}/sstate-cache"
[ ! -d downloads ] && ln -sf "${OE_CACHE_DIR}/downloads" .
[ ! -d sstate-cache ] && ln -sf "${OE_CACHE_DIR}/sstate-cache" .

# Retrieve build directory from persistent storage
[ -d "${PERMSTOR}/build-${MACHINE}/" ] && rsync -ax --delete \
  "${PERMSTOR}/build-${MACHINE}/" \
  "${WORKSPACE}/build-${MACHINE}/"

# The setup-environment-internal script does not care about
# unbound variables, nor exit codes. Setting +eu is acceptable
# from here onwards.
set +eu
source "${WORKSPACE}/setup-environment" "build-${MACHINE}"

[ -e "${WORKSPACE}/custom-kernel-info.inc.tmp" ] && mv -v "${WORKSPACE}/custom-kernel-info.inc.tmp" "conf/custom-kernel-info.inc"

bitbaker ${LKFT_IMAGES}
bbret=$?

# Store build directory in persistent storage
[ -d "${WORKSPACE}/build-${MACHINE}/" ] && rsync -ax --delete \
  --exclude bitbake-cookerdaemon.log \
  --exclude buildhistory \
  --exclude conf \
  --exclude tmp-lkft-glibc \
  "${WORKSPACE}/build-${MACHINE}/" \
  "${PERMSTOR}/build-${MACHINE}/"

[ ${bbret} -ne 0 ] && exit ${bbret}

# Disable network for AUTOREV
echo 'BB_SRCREV_POLICY = "cache"' >> conf/local.conf

# Save Bitbake environment
echo "Getting metadata for Bitbake..."
bb_env=$(mktemp)
bitbake -e > ${bb_env}
(
  grep "^GCCVERSION=" "${bb_env}"
  grep "^TARGET_SYS=" "${bb_env}"
  grep "^TUNE_FEATURES=" "${bb_env}"
  grep "^STAGING_KERNEL_DIR=" "${bb_env}"
) > "${WORKSPACE}/out/metadata-bitbake"
chmod 0644 "${WORKSPACE}/out/metadata-bitbake"
rm "${bb_env}"

# Generate LKFT metadata
:>"${WORKSPACE}/out/metadata-lkft"
for recipe in kselftests-mainline kselftests-next ltp libhugetlbfs "${KERNEL_RECIPE}"; do
  echo "Getting metadata for ${recipe}..."
  tmpfile=$(mktemp)
  pkg=$(echo "$recipe" | tr '[a-z]-' '[A-Z]_')
  bitbake -e "${recipe}" | grep -e ^PV= -e ^SRC_URI= -e ^SRCREV= > "${tmpfile}"
  source "${tmpfile}"
  for suri in $SRC_URI; do if [[ ! $suri =~ file:// ]]; then uri=$(echo "$suri" | cut -d\; -f1); export ${pkg}_URL="$uri"; break; fi; done
  export ${pkg}_VERSION="${PV}"
  export ${pkg}_REVISION="${SRCREV}"
  unset -v PV SRC_URI SRCREV
  rm "${tmpfile}"
  for v in URL VERSION REVISION; do
    myvar="${pkg}_${v}"
    echo "${myvar}=${!myvar}" >> "${WORKSPACE}/out/metadata-lkft"
  done
done

mkdir -p "${PERMSTOR}/images-out/"
bi=$(mktemp -d -p "${PERMSTOR}/images-out/")
BUILD_IMAGES=$(readlink -e "${bi}")
echo BUILD_IMAGES="${BUILD_IMAGES}" >> "${LKFT_CONF}"
rsync -ax "${WORKSPACE}/build-${MACHINE}"/tmp*/deploy/images/"${MACHINE}"/ "${BUILD_IMAGES}"/
