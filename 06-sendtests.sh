#!/bin/bash

source "$(readlink -e "$(dirname "$0")")/lib-lkft.sh"
lkft_init

config_dir=""
[ -v XDG_CONFIG_HOME ] && config_dir="${XDG_CONFIG_HOME}"
if [ -v LAVACLI_TOKEN ]; then
  config_dir=$(mktemp -d -p "${WORKSPACE}")
  cat > "${config_dir}/lavacli.yaml" << EOF
default:
  uri: https://lkft.validation.linaro.org/RPC2/
  username: ${LAVACLI_USER}
  token: ${LAVACLI_TOKEN}
EOF
fi
export config_dir

find "${WORKSPACE}/out/lava-jobs" -type f -name "*.yaml" | while read test; do
	lavajob=$(XDG_CONFIG_HOME="${config_dir}" lavacli jobs submit "${test}")
	echo "Submitted: ${lavajob}: ${test}"
done

if [ ! -v LAVACLI_TOKEN ]; then
  rm -rf "${config_dir}"
fi
