#!/bin/bash

# 01-kernel.sh
#
# This script will clone a kernel and define the needed information that
# other scripts can consume.
#
#
# Input:
#   KERNEL_REPO (environment variable)
#   KERNEL_REVISION (environment variable)
#
# The KERNEL_REPO can be any given Git repository for a kernel, such as:
#   https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
# The KERNEL_REVISION is any reference that Git can understand. Can be
# a tag, branch, commit ID.
#
# Output:
#   lkft.conf (file)
#     KERNEL_REPO
#     KERNEL_COMMIT
#     KERNEL_VERSION
#
# The lkft.conf file will pass the KERNEL_REPO as is, and will
# include the determined KERNEL_COMMIT for the repo/revision combination
# given as input. The KERNEL_VERSION is the major and minor versions
# derived from `make kernelversion`.
#
#
# Along with the specified repository, a few others will be made
# available for Git to take references from, namely:
#   torvalds - Linus Torvalds' mainline
#   linux-stable - Greg KH's stable tree
#   linux-stable-rc - Greg KH's stable release candidates tree

source "$(readlink -e "$(dirname "$0")")/lib-lkft.sh"
lkft_init

PERM_LINUX_DIR="${PERMSTOR}/linux"

echo "Initial working directory: $(pwd)"

check_var KERNEL_REPO error
check_var KERNEL_REVISION error

# $1 = remote name
# $2 = remote url
function git_add_remote()
{
  url=$(git remote get-url "$1" 2>/dev/null ||:)
  if [ -z "${url}" ]; then
    git remote add "$1" "$2"
  else
    if [ ! "${url}" = "$2" ]; then
      echo "WARNING: Remote $1 already exists with other URL (${url} instead of $2)."
      return 1
    fi
  fi
}

function write_lkft_config()
{
  (
    echo KERNEL_REPO="${KERNEL_REPO}"
    echo KERNEL_COMMIT="${KERNEL_COMMIT}"
    echo KERNEL_VERSION="${KERNEL_VERSION}"
    echo KERNEL_DESCRIBE="${KERNEL_DESCRIBE}"
    echo KERNEL_BRANCH="${KERNEL_BRANCH}"
    echo MAKE_KERNELVERSION="${KERNEL_MAKEVERSION}"
  ) > "${LKFT_CONF}"
  echo "Contents of LKFT config (${WORKSPACE}/out/lkft.conf):"
  cat "${LKFT_CONF}"
}

# Speed this up by working on permanent storage
if [ ! -d "${PERM_LINUX_DIR}" ]; then
  # Initial clone of kernel in persistent storage
  git clone -o origin https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git "${PERM_LINUX_DIR}"
fi

cd "${PERM_LINUX_DIR}"
echo "Current working directory: $(pwd)"
git remote | xargs -n1 git remote rm
git_add_remote origin "${KERNEL_REPO}"
git_add_remote torvalds https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
git_add_remote linux-stable https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
git_add_remote linux-stable-rc https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git
git fetch origin
git checkout "${KERNEL_REVISION}"

KERNEL_COMMIT="$(git rev-parse HEAD)"
KERNEL_DESCRIBE="$(git describe --always)"
KERNEL_MAKEVERSION="$(make kernelversion)"
kernel_major="$(echo "${KERNEL_MAKEVERSION}" | cut -d\. -f1)"
kernel_minor="$(echo "${KERNEL_MAKEVERSION}" | cut -d\. -f2)"
KERNEL_VERSION="${kernel_major}.${kernel_minor}"
KERNEL_BRANCH="$(git branch -r --contains "${KERNEL_COMMIT}" | grep origin | head -n1 | sed -e 's#^  origin/##')"
write_lkft_config
